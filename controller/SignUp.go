package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"formLs/model"
	httpResponse "formLs/utils"
	"net/http"
	"time"

	"crypto/sha256"
	"encoding/hex"

	"github.com/gorilla/mux"
	"github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

func NewUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("fsfdsasdfgd")
	var user model.User
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest, "Invalid JSON body")
		fmt.Println("oiiii")
		return
	}
	defer r.Body.Close()

	// Hash the password
	hashedPassword, err := hashPassword(user.Password)
	if err != nil {
		httpResponse.ResponseWithError(w, http.StatusInternalServerError, "Failed to hash password")
		return
	}
	user.Password = hashedPassword

	if user.IsAnonymous {
		// Hash the email and username for anonymous users
		hashedEmail := hashEmail(user.Email)
		user.Email = hashedEmail

	}

	saveErr := user.Create()
	if saveErr != nil {
		// Check for duplicate key violation error
		if pqErr, ok := saveErr.(*pq.Error); ok && pqErr.Code == "23505" {
			httpResponse.ResponseWithError(w, http.StatusBadRequest, "User alreusery exists")
			fmt.Println("hi")
			return
		}
		httpResponse.ResponseWithError(w, http.StatusBadRequest, saveErr.Error())
		fmt.Println("HELLO", saveErr)
		return
	}
	httpResponse.ResponseWithJSON(w, http.StatusCreated, map[string]string{"status": "User userded"})
}

var user model.User

func Login(w http.ResponseWriter, r *http.Request) {
	fmt.Println("hi there")

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest, "Invalid JSON body")
		return
	}

	// Retrieve the user from the database
	// dbUser := model.User{Email: user.Email}
	fmt.Println("emial", user.Email)

	if user.IsAnonymous {
		client, getErr := user.Get()
		if getErr != nil {
			if getErr == sql.ErrNoRows {
				httpResponse.ResponseWithError(w, http.StatusUnauthorized, "Invalid")
				fmt.Println("error from email", getErr)
				return
			} else {
				httpResponse.ResponseWithError(w, http.StatusInternalServerError, "Error with database")
				fmt.Println(getErr)
			}
			return
		}
		//create a cookie
		cookie := http.Cookie{
			Name: "User-cookie",
			// Value: email +User.Password,
			Value:   "my-value",
			Expires: time.Now().Add(30 * time.Minute),
			Secure:  true,
		}
		//set cookie and send back to client
		http.SetCookie(w, &cookie)
		httpResponse.ResponseWithJSON(w, http.StatusOK, map[string]string{"message": "successful"})
		fmt.Println("Login Successful")
		httpResponse.ResponseWithJSON(w, http.StatusOK, client)
		fmt.Println(client)
	} else {
		email := user.Email
		var user1 model.User
		getErr := user1.GetUser(email)
		if getErr != nil {
			switch getErr {
			case sql.ErrNoRows:
				httpResponse.ResponseWithError(w, http.StatusUnauthorized, "invalid login")
				fmt.Println("error with email")
			default:
				httpResponse.ResponseWithError(w, http.StatusInternalServerError, "error with the database")
				fmt.Println("get error", getErr)
			}
			return
		}
		// Compare the hashed password
		err = comparePasswords(user.Password, user1.Password)
		if err != nil {
			fmt.Println("Comparison error:", err)
			httpResponse.ResponseWithError(w, http.StatusUnauthorized, "Invalid password")
			return
		}
		cookie := http.Cookie{
			Name: "User-cookie",
			// Value: email +User.Password,
			Value:   "my-value",
			Expires: time.Now().Add(30 * time.Minute),
			Secure:  true,
		}
		//set cookie and send back to client
		http.SetCookie(w, &cookie)
		httpResponse.ResponseWithJSON(w, http.StatusOK, map[string]string{"message": "successful"})
		fmt.Println("reaching")
	}

}

func hashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hashedPassword), nil
}

func comparePasswords(password, hashedPassword string) error {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	if err != nil {
		return err
	}
	return nil
}

func hashEmail(email string) string {
	hasher := sha256.New()
	hasher.Write([]byte(email))
	hashedEmail := hasher.Sum(nil)
	hashedEmailString := hex.EncodeToString(hashedEmail)
	return hashedEmailString
}

// func compareHashedEmail(email, hashedEmail string) bool {
// 	hashedProvidedEmail := hashEmail(email)
// 	return hashedProvidedEmail == hashedEmail
// }

// func compareHashedUsername(username, hashedUsername string) bool {
// 	// Hash the provided username using the same algorithm as the original hashing
// 	hasher := sha256.New()
// 	hasher.Write([]byte(username))
// 	hashedProvidedUsername := hex.EncodeToString(hasher.Sum(nil))

//		// Compare the hashed provided username with the original hashed username
//		return hashedProvidedUsername == hashedUsername
//	}
func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:    "User-cookie",
		Expires: time.Now(),
	})
	fmt.Println("logout successful")
	httpResponse.ResponseWithJSON(w, http.StatusOK, map[string]string{"message": "logout successful"})
}

func VerifyCookie(w http.ResponseWriter, r *http.Request) bool {
	cookie, err := r.Cookie("User-cookie")
	if err != nil {
		switch err {
		case http.ErrNoCookie:
			httpResponse.ResponseWithError(w, http.StatusSeeOther, "cookie not set")
		default:
			httpResponse.ResponseWithError(w, http.StatusInternalServerError, "internal server error")
		}
		return false
	}
	fmt.Println(user.Email, "email")
	fmt.Println(user.Password, "password")
	if cookie.Value != "my-value" {
		httpResponse.ResponseWithError(w, http.StatusSeeOther, "invalid cookie")
		return false
	}
	return true
}

func GetUserHandler(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	user := model.User{Email: email}
	getErr := user.GetSignup()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			fmt.Println("user not found")
			httpResponse.ResponseWithError(w, http.StatusNotFound, "user not found")
		default:
			httpResponse.ResponseWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	} else {
		httpResponse.ResponseWithJSON(w, http.StatusOK, user)
	}
}

func DeleteOwnerHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("entered DeletOwner handler")
	email := mux.Vars(r)["email"]
	prop := model.User{Email: email}
	fmt.Println(email)
	err := prop.DeleteOwner()
	if err != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest, err.Error())
		fmt.Println(err)
		return
	}
	fmt.Println("DELETE")
	httpResponse.ResponseWithJSON(w, http.StatusOK, map[string]string{"status": "deleted"})
}

func UpdateOwenerHandler(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	var owner model.User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&owner)
	if err != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest, "invilid json body")
		return
	}

	updateTitleErr := owner.UpdateOwnerProfile(email)
	if updateTitleErr != nil {
		switch updateTitleErr {
		case sql.ErrNoRows:
			httpResponse.ResponseWithError(w, http.StatusNotFound, "owner not found")
		default:
			httpResponse.ResponseWithError(w, http.StatusInternalServerError, updateTitleErr.Error())
		}
	} else {
		httpResponse.ResponseWithJSON(w, http.StatusOK, owner)
	}
}
