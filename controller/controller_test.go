package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLogin(t *testing.T) {
	url := "http://127.0.0.1:8080/login"
	var data = []byte(`{"Email":"d@gmail.com","Password":"d","IsAnonymous":false}`)
	// Create request object
	r, _ := http.NewRequest("POST", url, bytes.NewBuffer(data))
	r.Header.Set("Content-type", "application/json")
	// Create client
	client := &http.Client{}
	// Send post request, specified by the method inside r
	resp, err := client.Do(r)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)

	assert.Equal(t, http.StatusOK, resp.StatusCode)
	expResp := `{"message":"successful"}`
	assert.JSONEq(t, expResp, string(body))
}

func TestGetContent(t *testing.T) {
	c := http.Client{}
	r, err := c.Get("http://localhost:8080/content/7ibcn")
	if err != nil {
		panic(err)
	}
	body, bErr := io.ReadAll(r.Body)
	if bErr != nil {
		panic(bErr)
	}
	assert.Equal(t, http.StatusOK, r.StatusCode)
	assert.JSONEq(t, `{"ctid":"7ibcn","post":"fdas","email":"i@gmail.com"}`, string(body))
}

func TestDeleteContent(t *testing.T) {
	url := "http://127.0.0.1:8080/content/7ibcn"
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		panic(err)
	}
	client := &http.Client{}
	resp, rErr := client.Do(req)
	if rErr != nil {
		panic(rErr)
	}
	defer resp.Body.Close()

	body, bErr := io.ReadAll(resp.Body)
	if bErr != nil {
		panic(bErr)
	}
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	assert.JSONEq(t, `{"status":"deleted"}`, string(body))

}
