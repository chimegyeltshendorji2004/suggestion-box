package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"formLs/model"
	httpResponse "formLs/utils"
	"net/http"

	"github.com/gorilla/mux"
)

func AddContent(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	email := mux.Vars(r)["email"]
	var cont model.Content
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&cont)
	if err != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	fmt.Println("user", cont)
	defer r.Body.Close()
	saveErr := cont.Create(email)
	if saveErr != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	// no error
	httpResponse.ResponseWithJSON(w, http.StatusCreated, map[string]string{"Status": "Content Added"})
}

func GetContent(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	// get url parameter
	ctid := mux.Vars(r)["cid"]
	cont := model.Content{ContentId: ctid}
	getErr := cont.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResponse.ResponseWithError(w, http.StatusNotFound, "Content not found")
		default:
			httpResponse.ResponseWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	} else {
		httpResponse.ResponseWithJSON(w, http.StatusOK, cont)
	}
	// fmt.Println(stud)
}

func UpdateContent(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	ctid := mux.Vars(r)["cid"]
	var cont model.Content
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&cont)
	if err != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest, "invalid body")
		return
	}
	updateErr := cont.Update(ctid)
	if updateErr != nil {
		switch updateErr {
		case sql.ErrNoRows:
			httpResponse.ResponseWithError(w, http.StatusNotFound, "Content Not Found")
		default:
			httpResponse.ResponseWithError(w, http.StatusInternalServerError, updateErr.Error())
		}
	} else {
		httpResponse.ResponseWithJSON(w, http.StatusOK, cont)
	}
}

func DeleteContent(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	ctid := mux.Vars(r)["cid"]
	c := model.Content{ContentId: ctid}
	if err := c.Delete(); err != nil {
		switch err {
		case sql.ErrNoRows:
			httpResponse.ResponseWithError(w, http.StatusBadRequest, "Content has been either deleted or doesnt exist")
		default:
			httpResponse.ResponseWithError(w, http.StatusInternalServerError, err.Error())
		}
		// return
	} else {
		httpResponse.ResponseWithJSON(w, http.StatusOK, map[string]string{"status": "deleted"})
	}
}

func GetAllContent(w http.ResponseWriter, r *http.Request) {
	if !VerifyCookie(w, r) {
		return
	}
	email := mux.Vars(r)["email"]
	conts, getErr := model.GetAllContent(email)
	if getErr != nil {
		httpResponse.ResponseWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResponse.ResponseWithJSON(w, http.StatusOK, conts)
}
