package routes

import (
	"fmt"
	"formLs/controller"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func InitializeRoutes() {
	router := mux.NewRouter()

	router.HandleFunc("/register", controller.NewUser).Methods("POST")
	router.HandleFunc("/login", controller.Login).Methods("POST")
	router.HandleFunc("/logout", controller.LogoutHandler).Methods("GET")

	router.HandleFunc("/user/{email}", controller.GetUserHandler).Methods("GET")
	router.HandleFunc("/deluser/{email}", controller.DeleteOwnerHandler).Methods("DELETE")
	router.HandleFunc("/user/{email}", controller.UpdateOwenerHandler).Methods("PUT")

	router.HandleFunc("/content/{email}", controller.AddContent).Methods("POST")
	router.HandleFunc("/content/{cid}", controller.GetContent).Methods("GET")
	router.HandleFunc("/content/{cid}", controller.UpdateContent).Methods("PUT")
	router.HandleFunc("/content/{cid}", controller.DeleteContent).Methods("DELETE")
	router.HandleFunc("/contents", controller.GetAllContent)

	fhandler := http.FileServer(http.Dir("./view")) //Dir is a type
	router.PathPrefix("/").Handler(fhandler)

	err := http.ListenAndServe(":8081", router)
	log.Println("Application is running on port 8080........")
	if err != nil {
		fmt.Println("problem with listenandserve", err)
		os.Exit(1)
	}
}
