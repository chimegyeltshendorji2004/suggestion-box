package model

import (
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"fmt"
	users "formLs/dataStore"
)

type User struct {
	Uimg        string
	UserName    string
	Email       string
	Password    string
	IsAnonymous bool
}

const queryInsertUser = "INSERT INTO Users (Uimg, UserName, Email, Password,anonymity) VALUES($1,$2,$3,$4,$5);"

func (u *User) Create() error {
	_, err := users.Db.Exec(queryInsertUser, u.Uimg, u.UserName, u.Email, u.Password, u.IsAnonymous)
	return err
}

const queryGetUser = "SELECT * FROM users;"

func (u *User) Get() (*User, error) {
	users.Db.Query(queryGetUser)
	// Hash the provided email
	hashedEmail := hashEmail(u.Email)

	// Retrieve all users from the database
	rows, err := users.Db.Query("SELECT * FROM users")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var user User
		err := rows.Scan(&user.Uimg, &user.UserName, &user.Email, &user.Password, &user.IsAnonymous)
		fmt.Println("model", user)
		if err != nil {
			return nil, err
		}

		// Compare the hashed email with the email retrieved from the database
		if hashedEmail == user.Email {
			return &user, nil
		}
	}
	// No matching user found
	return nil, sql.ErrNoRows
}

func hashEmail(email string) string {
	hasher := sha256.New()
	hasher.Write([]byte(email))
	hashedEmail := hasher.Sum(nil)
	hashedEmailString := hex.EncodeToString(hashedEmail)
	return hashedEmailString
}
func (u *User) GetUser(email string) error {
	const query = "select * from users where Email = $1;"
	return users.Db.QueryRow(query, email).Scan(&u.Uimg, &u.UserName, &u.Email, &u.Password, &u.IsAnonymous)
}

func (u *User) GetSignup() error {
	err := users.Db.QueryRow("select * from users where Email = $1;", u.Email).Scan(&u.Uimg, &u.UserName, &u.Email, &u.Password, &u.IsAnonymous)
	return err
}
func (u *User) DeleteOwner() error {
	if err := users.Db.QueryRow("DELETE from users WHERE Email = $1", u.Email).Scan(&u.Email); err != nil {
		return err
	}
	return nil
}
func (u *User) UpdateOwnerProfile(email string) error {
	err := users.Db.QueryRow("UPDATE users set Uimg = $1, UserName =$2, Password = $3 RETURNING Email", u.Uimg, u.UserName, u.Password).Scan(&u.Email)
	return err
}
