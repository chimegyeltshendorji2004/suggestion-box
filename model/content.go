package model

import users "formLs/dataStore"

type Content struct {
	ContentId string `json:"ctid"`
	Post      string `json:"post"`
	Email     string `json:"email"`
}

const (
	queryContentInsert  = "INSERT INTO content(contentid, post, email) VALUES($1, $2, $3);"
	queryContentGetUser = "SELECT contentid, post,email FROM content WHERE contentid=$1;"
	queryContentUpdate  = "UPDATE content SET contentid=$1, post=$2 WHERE contentid=$3 RETURNING contentid;"
	queryContentDelete  = "DELETE FROM content WHERE contentid=$1 RETURNING contentid;"
	queryContentSelect  = "SELECT * from content;"
)

func (c *Content) Create(email string) error {
	_, err := users.Db.Exec(queryContentInsert, c.ContentId, c.Post, email)
	return err
}

func (c *Content) Read() error {
	return users.Db.QueryRow(queryContentGetUser, c.ContentId).Scan(&c.ContentId, &c.Post, &c.Email)
}

func (c *Content) Update(ctid string) error {
	row := users.Db.QueryRow(queryContentUpdate, c.ContentId, c.Post, ctid)
	err := row.Scan(&c.ContentId)
	return err
}

func (c *Content) Delete() error {
	row := users.Db.QueryRow(queryContentDelete, c.ContentId)
	err := row.Scan(&c.ContentId)
	if err != nil {
		return err
	}
	return nil
}

func GetAllContent(string) ([]Content, error) {
	rows, getErr := users.Db.Query(queryContentSelect)
	if getErr != nil {
		return nil, getErr
	}

	contents := []Content{}

	for rows.Next() {
		var c Content
		dbErr := rows.Scan(&c.ContentId, &c.Post, &c.Email)
		if dbErr != nil {
			return nil, dbErr
		}
		contents = append(contents, c)
	}
	rows.Close()
	return contents, nil
}
