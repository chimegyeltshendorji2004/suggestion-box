package main

import (
	routes "formLs/routes"
)

func main() {
	routes.InitializeRoutes()
}
