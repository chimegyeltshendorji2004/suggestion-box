let email = localStorage.getItem("email")
// alert(email, "hiii")

function addContent() {
    var data = getFormData();
    var cid = data.ctid;
    fetch('/content/' + email, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {"Content-type": "application/json; charset=UTF-8"}
    }).then(response1 => {
        if (response1.ok) {
            fetch('/content/' + cid)
            .then(response2 => response2.text())
            .then(data => showContent(data))
            .then(() => {
                // Hide body1 and show body2
                document.querySelector('.body1').style.display = 'none';
                document.querySelector('.body2').style.display = 'flex';
            });
        } else {
            throw new Error(response1.statusText);
        }
    }).catch(e => {
        alert(e);
    });
    resetform();
  }
 
  window.onload = function() {
    fetch("/user/"+email)
      .then(rosponse => rosponse.text())
      .then(data => showUserData(data))

      fetch('/contents')
      .then(response => response.text())
      .then(data => showContents(data));

  }
  
  function showContent(data) {
      const content = JSON.parse(data)
      newRow(content)
  }
  
  function showContents(data) {
      const contents = JSON.parse(data)
      contents.forEach(stud => {
          newRow(stud)
      })
  }
  
  // Helper Function to Create newrow and cells
  function newRow(content) {
        console.log(content)
        var table = document.getElementById("myTable");
        var row = table.insertRow(table.length);
        var td = []
        for (i = 0; i < table.rows[0].cells.length; i++){
            td[i] = row.insertCell(i);
            td[i].classList.add("tableContent"+i)
        }
        td[0].innerHTML = content.ctid
        td[1].innerHTML = content.post;
        var loggedData = JSON.parse(localStorage.getItem("loggedData"))
        console.log(content.email,"content")
        console.log(loggedData.Email,"loggedData")
       
        if (loggedData.Email === content.email){
            td[2].innerHTML = '<input type="button" onclick="updateContent(this)"value="Edit" id="button-2">';
            td[3].innerHTML = '<input type="button" onclick="deleteContent(this)"value="Delete" id="button-1">';
        }
  }
  
  function resetform() {
      // document.getElementById("cid").value = "";
      document.getElementById("post").value = "";
  }
  
  var selectedRow = null
  function updateContent(r) {
      selectedRow = r.parentElement.parentElement;
      // document.getElementById("cid").value = selectedRow.cells[0].innerHTML;
      document.getElementById("post").value = selectedRow.cells[1].innerHTML;
  
      var btn = document.getElementById("button-add");
      cid = selectedRow.cells[0].innerHTML;
      if (btn) {
          btn.innerHTML = "Update";
          btn.setAttribute("onclick", "update(cid)")
  
          document.querySelector('.body2').style.display = 'none';
          document.querySelector('.body1').style.display = 'flex';
      }
  }
  
  var result; // Declare 'result' as a global variable
  
  function assignNewValueToResult() {
    result = Math.random().toString(36).substring(2, 7);
    return result
  }
  
  // Helper Function for get data
  function getFormData() {
      var formData = {
          ctid : assignNewValueToResult(),
          post : document.getElementById("post").value,
      }
      return formData
  }
  
  function update(cid) {
      var newData = getFormData()
  
      fetch('/content/' + cid, {
          method: "PUT",
          body: JSON.stringify(newData),
          headers: {"Content-type": "application/json; charset=UTF-8"}
      }).then (res => {
          if (res.ok) {
              // Fill the selected row with new value
              // selectedRow.cells[0].innerHTML = newData.ctid;
              selectedRow.cells[1].innerHTML = newData.post;
  
              // Set to previous value
              var button = document.getElementById("button-add");
              button.innerHTML = "Post";
              button.setAttribute("onclick", "addContent()");
              selectedRow = null;
  
              resetform()
          } else {
              alert("Server: Update request error.")
          }
      })
  }
  
  function deleteContent(r) {
      if (confirm("Are you sure you want to DELETE this?")) {
          selectedRow = r.parentElement.parentElement;
          cid = selectedRow.cells[0].innerHTML;
  
          fetch('/content/' + cid, {
              method: "DELETE",
              headers: {"Content-type": "application/json; charset=UTF-8"}
          });
          var rowIndex = selectedRow.rowIndex;
          if (rowIndex>0) {
              document.getElementById("myTable").deleteRow(rowIndex);
          }
          selectedRow = null;
      }
  }
  
  function viewPost() {
    document.querySelector('.body2').style.display = 'flex';
    document.querySelector('.body1').style.display = 'none';
  }
  
  function makePost() {
    document.querySelector('.body2').style.display = 'none';
    document.querySelector('.body1').style.display = 'flex';
    window.location.reload()
  }
  
  function updateButtonState() {
      var postInput = document.getElementById('post');
      var addButton = document.getElementById('button-add');
    
      if (postInput.value !== '') {
        addButton.disabled = false;
      } else {
        addButton.disabled = true;
      }
    }
    
  document.getElementById('post').addEventListener('input', updateButtonState);


function showUserData(data){
    let userData = JSON.parse(data)
    localStorage.setItem("loggedData",data)
    loggedData = JSON.parse(localStorage.getItem("loggedData"))
    // alert("hi")
    console.log(loggedData)
    document.querySelector("#i").src = userData.Uimg
    document.querySelector(".n").innerHTML = userData.UserName
    // document.querySelector(".e").innerHTML = email
    // document.querySelector(".p").innerHTML = userData.Password 
}