let email = localStorage.getItem("email")



const imgDiv = document.querySelector('.profile-pic-div');
const img = document.querySelector('#photo');
const file = document.querySelector('#file');
const uploadBtn = document.querySelector('#uploadBtn');

imgDiv.addEventListener('mouseenter', function(){
    uploadBtn.style.display = "block";
});


imgDiv.addEventListener('mouseleave', function(){
    uploadBtn.style.display = "none";
});

file.addEventListener('change', function(){
    const choosedFile = this.files[0];
    if (choosedFile) {
        const reader = new FileReader();
        reader.addEventListener('load', function(){
            img.setAttribute("src", reader.result);
            localStorage.setItem("imgUrl", reader.result);

        });
        reader.readAsDataURL(choosedFile);
    }
});

function userSave() {
    // alert(localStorage.getItem("imgUrl"))
    newData = {
        Uimg : localStorage.getItem("imgUrl"),
        UserName : document.getElementById("name").value,
        Password :document.getElementById("password").value
    }
    // alert("hello")
    fetch("/user/"+email ,{
        method: "PUT",
        body: JSON.stringify(newData),
        headers: {"Content-type": "application/json; charset=UTF-8"}
    })
    .then(response => {
        if (response.status === 200) {
            alert("Your profile has been updated")
          window.location.href = "profile2.html";
        } else if (response.status === 401) {
          alert("Invalid login, try again");
        } else {
          throw new Error(response.statusText);
        }
      })
      .catch(error => {
        if (error.message === "Unauthorized") {
          alert(error.message + ". Credentials do not match!");
        }
      });
}
