
const checkbox = document.getElementById('anonymousCheckbox');
let i = false;
checkbox.addEventListener('change', () => {
  if (checkbox.checked) {
    i = true;
    // console.log(true);
  } else {
    i = false;
    // console.log(false);
  }
  // console.log(i);
});
i = false;
function login() {
  var email = document.getElementById("user_email_login").value.trim();
  var password = document.getElementById("user_pass_login").value.trim();
  var anonymousCheckbox = document.getElementById("anonymousCheckbox");

  // If the user is logging in anonymously, set a placeholder email and password


  if (!email || !password) {
    alert("Please fill in all the form fields.");
    return;
  }

  var data = {
    Email: email,
    Password: password,
    IsAnonymous: i
  };

  fetch('/login', {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "Content-type": "application/json; charset=UTF-8" }
  })
    .then(response => {
      if (response.status === 200) {
        localStorage.setItem("email", email)
        window.location.href = "content.html";
      } else if (response.status === 401) {
        alert("Invalid login, try again");
      } else {
        throw new Error(response.statusText);
      }
    })
    .catch(error => {
      if (error.message === "Unauthorized") {
        alert(error.message + ". Credentials do not match!");
      }
    });
}
function logout(){
  fetch('/logout')
  .then(response => {
    if (response.ok){
      window.location.href = "index.html"
    }else{
      throw new Error(response.statusText)
    }

  })
  .catch(e=>{
    alert (e)
  })
}