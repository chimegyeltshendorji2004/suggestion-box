const checkbox = document.getElementById('anonymousCheckbox');
let i = false;
checkbox.addEventListener('change', () => {
  if (checkbox.checked) {
    i = true;
    // console.log(true);
  } else {
    i = false;
    // console.log(false);
  }
  // console.log(i);
});
i = false;
function validateForm() {
  var userName = document.getElementById("user_name").value;
  var email = document.getElementById("user_email_SignUp").value;
  var password1 = document.getElementById("user_pass_signUp1").value;
  var password2 = document.getElementById("user_pass_signUp2").value;

  if (userName === "" && email === "" && password1 ==="" && password2 === "") {
    alert("Please enter all the credentials")
  }else if(email === "") {
    alert("Email is required.");
  } else if (!isValidEmail(email)) {
    alert("Invalid email format.");
  }else if(password1 === "") {
    alert("Password is required.");
  }else if(password2 === "") {
    alert("Repeat password is required.");
  } else if (password1 !== password2) {
    alert("Passwords do not match.");
  }else if (userName === "") {
    alert("Username is required.");
  } else {
    return true;
  }
  
}

function isValidEmail(email) {
  // A simple email format validation regex
  var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailRegex.test(email);
}

function togglePasswordVisibility(inputId) {
  var passwordInput = document.getElementById(inputId);
  var passwordToggleIcon = document.querySelector('[for="' + inputId + '"]::before');

  if (passwordInput.type === "password") {
    passwordInput.type = "text";
    passwordToggleIcon.classList.remove("fa-eye-slash");
    passwordToggleIcon.classList.add("fa-eye");
  } else {
    passwordInput.type = "password";
    passwordToggleIcon.classList.remove("fa-eye");
    passwordToggleIcon.classList.add("fa-eye-slash");
  }
}
function togglePasswordVisibility(inputId) {
  var input = document.getElementById(inputId);
  if (input.type === "password") {
    input.type = "text";
  } else {
    input.type = "password";
  }
}

function signUp() {

  if (validateForm()) { 
  var username = document.getElementById("user_name").value.trim();
  var email = document.getElementById("user_email_SignUp").value.trim();
  var password1 = document.getElementById("user_pass_signUp1").value;
  var isAnonymous = document.getElementById("anonymousCheckbox").checked;

  if (isAnonymous) {
    username = "Anonymous";
    document.getElementById("user_name").value = username;
    document.getElementById("user_name").disabled = true;
    document.getElementById("user_email_SignUp").disabled = true;
  }

  var data = {
    UserName: username,
    Email: email,
    Password: password1,
    IsAnonymous: i
  };
  console.log(data)
  
  // Send the signup data to the server
  

  fetch('/register', {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "Content-type": "application/json; charset=UTF-8" }
  })
  .then(response => {
    if (response.status === 201) {
      console.log('sdfsd')
      // Redirect the user to the login page
      window.location.href = "index.html";
    } else if (response.status === 400) {
      alert("User with the provided email already exists. Please choose a different email.");
    } else {
      throw new Error(response.statusText);
    }
  })
  .catch(error => {
    console.error(error);
  });
  }else {
    alert("try agiang")
  }



}

